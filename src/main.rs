extern crate libc;

mod git;

fn main() {

    let path = std::env::args().skip(1).next().expect("usage: git_wrapper PATH");

    let repo = git::Repository::open(&path).expect("openining repository");

    let commit_oid = repo.reference_name_to_id("HEAD").expect("looking up 'HEAD' reference");

    let commit = repo.find_commit(&commit_oid).expect("looking up commit");

    let author = commit.author();
    println!("{} <{}>\n", author.name().unwrap_or("None"), author.email().unwrap_or("None"));

    println!("{}", commit.message().unwrap_or("(none)"));

}

